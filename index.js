const express = require('express');
const bodyParser = require('body-parser');
const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

let value = [];

app.get('/api', (req, res) => {
    res.send({
        hello: 'world'
    });
});

app.post('/api', (req, res) => {
    let user_input = req.body.user_input;
    value.push(user_input);
    res.status(201).send({
        input: value
    });
    //console.log(user_input);
})

app.listen('3000', () => {
    console.log('Server running on port 3000')
})